#include <stdio.h>

int main()
{
	const int length = 5;
	int i;
	int numbers[length] = {3, 5, 1, 2, 4};
	int swap;
	int loopControl = length - 1;

	for(i = 0; i < length; i++)
		printf("%d ", numbers[i]);
	
	printf("\n");

	while(loopControl > 0)
	{
		for (i = 0; i < length; i++)
		{
			if (i+1 < length && numbers[i] > numbers[i+1])
			{
				swap = numbers[i];
				numbers[i] = numbers[i+1];
				numbers[i+1] = swap;
			}
		}

		for(i = 0, loopControl = length - 1; i < length; i++){
			printf("%d ", numbers[i]);
			if (i+1 < length && numbers[i] < numbers[i+1])
				--loopControl;
		}
		printf("\n");


	}



	return 0;
}